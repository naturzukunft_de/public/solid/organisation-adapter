package de.naturzukunft.rdf.solid.organisation.adapter;

import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import de.naturzukunft.rdf.solid.pod.adapter.BuilderFactory;
import de.naturzukunft.rdf.solid.pod.adapter.DPopProvider;

@Component
public class PodStorageFactory {

	private BuilderFactory builderFactory;
	private DPopProvider dPopProvider;
	private WebClient webClient;

	public PodStorageFactory(BuilderFactory builderFactory, DPopProvider dPopProvider, WebClient webClient) {
		this.builderFactory = builderFactory;
		this.dPopProvider = dPopProvider;
		this.webClient = webClient;
	}
	
	public Storage getStorage(AuthInfoData authInfoData) {
		return new PodStorage(authInfoData, builderFactory, dPopProvider, webClient);
	}
}
