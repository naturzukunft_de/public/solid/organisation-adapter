package de.naturzukunft.rdf.solid.organisation.adapter;

import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.util.StringUtils;

import com.github.slugify.Slugify;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Organisation {
	private String webId;
	
	@NotNull
    @NotEmpty
	private String name;

	private String note; // v:note — Notes about a person on a vCard.
	
	private String geoUri; //https://en.wikipedia.org/wiki/Geo_URI_scheme
		
	private String countryNname;//The v:country-name property specifies the country of a postal address.
	private String locality; //The v:locality property specifies the locality (e.g., city) of a postal address.
	private String postalCode;//The v:postal-code property specifies the postal code (e.g., U.S. ZIP code) of a postal address.
	private String region; //The v:region property specifies the region (e.g., state or province) of a postal address.
	private String streetAddress; //The v:street-address property specifies the street address of a postal address.
	
//	  "contact_name": "John Smith",

	private String mobileEmail;
	private String personalEmail;
	private String unlabeledEmail;
	private String workEmail;
	
	private String unlabeledPhone;
	private String homePhone;
	private String mobilePhone;
	private String workPhone;
	private String fax;
	
	private String homepage; // http://xmlns.com/foaf/spec/#term_homepage 

//	The v:url property specifies a URL associated with a person.
	// kann man eine liste von urls haben ?
//	  "links": [
//    {
//      "url": "https://www.slowtec.de/",
//      "title": "string",
//      "description": "string"
//    }
//  ],

	
//	  "founded_on": {},

	private List<String> categories; 

	
//	Property: foaf:logo
//	logo - A logo representing some thing. 

	private String logoUrl;
	private String logoLinkUrl;
	
//	  "license": "CC0-1.0"
	
	public void setCategories(String csv) {
		this.categories = List.of(csv.split(","));
	}
	
	public String slugifiedOrganistionName() {
		return new Slugify().slugify(getName());
	}

	public boolean hasLogo() {
		return StringUtils.hasText(getLogoLinkUrl()) || StringUtils.hasText(getLogoUrl());
	}

	public boolean hasAddress() {
		return StringUtils.hasText(getStreetAddress()) ||
				StringUtils.hasText(getPostalCode()) ||
				StringUtils.hasText(getCountryNname()) ||
				StringUtils.hasText(getLocality()) ||
				StringUtils.hasText(getRegion());
	}

	public boolean hasCategories() {
		return this.categories != null && !this.categories.isEmpty();
	}
}
