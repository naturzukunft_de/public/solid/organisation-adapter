package de.naturzukunft.rdf.solid.organisation.adapter;

import java.net.URL;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuthInfoData {
	private URL webId;
	private String accessToken;
	private String clientId;
}
