package de.naturzukunft.rdf.solid.organisation.adapter;

import java.io.StringReader;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Selector;
import org.apache.jena.rdf.model.SimpleSelector;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class OrganisationsRepository {

    private OrganisationMapper organisationMapper; 

    public OrganisationsRepository(OrganisationMapper organisationMapper) {
		this.organisationMapper = organisationMapper;
    }
    
	public List<Organisation> findAll(Storage storage, URL webId) {
		List<Organisation> result = new ArrayList<>();
		URL organisationUrl = getOrganisationUrl(webId);

		storage.getContainerItems(organisationUrl).stream().parallel().forEach(url -> {
			try {
				URL profileUrl = addProfilePath(url);
				result.add(organisationMapper.toOrganisation(profileUrl, readModel(storage, organisationUrl, profileUrl)));
			} catch (WebClientResponseException e) {
				log.error("ignoring organisation: " + e.getMessage());
			}
		});
		return result;
	}

	private org.apache.jena.rdf.model.Model readModel(Storage storage, URL organisationUrl, URL profileUrl) {
		org.apache.jena.rdf.model.Model profileModel = ModelFactory.createDefaultModel();
		profileModel.read(new StringReader(storage.read(profileUrl)), organisationUrl.toString(), "TTL");
		return profileModel;
	}

	private URL getOrganisationUrl(URL webId) {
		String orgaisationUrlAsString = getOrgaisationUrlAsString(webId);
		URL url;
		try {
			url = new URL(orgaisationUrlAsString);
		} catch (MalformedURLException e) {
			throw new RuntimeException("error creating url for " + orgaisationUrlAsString, e);
		}
		return url;
	}

	private URL getOrgaisationUrl(URL webId) {
		try {
			return new URL(getOrgaisationUrlAsString(webId));
		} catch (MalformedURLException e) {
			throw new RuntimeException("error creating url for " + getOrgaisationUrlAsString(webId), e);
		}
	}

	private String getOrgaisationUrlAsString(URL webId) {
		String organisationsUrl = StringUtils.replace(webId.toString(), "profile/card#me", "public/organisations/");
		return organisationsUrl;
	}

	private URL addProfilePath(URL url) {
		String newUrlString = url.toString() + "profile";
		URL result;
		try {			
			result = new URL(newUrlString);
		} catch (MalformedURLException e) {
			throw new RuntimeException("error creating url for " + newUrlString, e);
		}
		return result;
	}

	private URL addPath(URL url, String path) {
		String baseUrl = url.toString();
		if(!baseUrl.endsWith("/")) {
			baseUrl += "/";
		}
		String newUrlString = baseUrl + path;
		URL result;
		try {			
			result = new URL(newUrlString);
		} catch (MalformedURLException e) {
			throw new RuntimeException("error creating url for " + newUrlString, e);
		}
		return result;
	}

	public List<Organisation> findByName(Storage storage, URL webId, String filtered) {
		List<Organisation> result = new ArrayList<>();
		URL organisationUrl = getOrganisationUrl(webId);

		
		for (URL url : storage.getContainerItemsFilteredBySelector(organisationUrl, organisationNameSelector(filtered))) {
			URL profileUrl = addProfilePath(url);
			result.add(organisationMapper.toOrganisation(profileUrl, readModel(storage, organisationUrl, profileUrl)));
		}
		return result;
	}

	private Selector organisationNameSelector(String name) {
		return new
		      SimpleSelector(null, LDP.contains, (RDFNode) null) {
			  public boolean selects(Statement s)
		        {
				  URI uri = URI.create(s.getObject().toString());
				  String token = uri.getPath();
				  if(token.endsWith("/")) {
					  token = token.substring(0, token.length()-1);
				  }
				  token = token.substring(token.lastIndexOf("/"), token.length());
				  return token.contains(name);
				 }
		     };
	}

	public void delete(Storage storage, URL webId, Organisation organisation) {
		URL organisationsUrl = getOrgaisationUrl(webId);
		URL orgUrl = addPath(organisationsUrl, organisation.slugifiedOrganistionName());
		URL urlToUse = addPath(orgUrl, "profile");
		storage.delete(urlToUse);
		storage.delete(orgUrl);
	}

	public void save(Storage storage, URL webId, Organisation organisation) {
		
		URL organisationsUrl = getOrgaisationUrl(webId);
		URL orgUrl = addPath(organisationsUrl, organisation.slugifiedOrganistionName());
		URL urlToUse = addPath(orgUrl, "profile");
		Model organisationAsModel = organisationMapper.toModel(webId, urlToUse, organisation);
		
		storage.store(modelToString(organisationAsModel), urlToUse, organisation.slugifiedOrganistionName()); 
	}

	private String modelToString(org.apache.jena.rdf.model.Model model) {
		StringWriter sw = new StringWriter();
		RDFDataMgr.write(sw, model, Lang.TURTLE);
		return sw.toString();
	}

}
