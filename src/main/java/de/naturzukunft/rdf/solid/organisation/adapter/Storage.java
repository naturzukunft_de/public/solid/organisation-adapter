package de.naturzukunft.rdf.solid.organisation.adapter;

import java.net.URL;
import java.util.List;

import org.apache.jena.rdf.model.Selector;

public interface Storage {

	List<URL> getContainerItems(URL namespace);
	List<URL> getContainerItemsFilteredBySelector(URL namespace, Selector selector);
	String read(URL namespace);
	String delete(URL namespace);
	String store(String content, URL namespace, String slug);
}
